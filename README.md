# Volume Ransomware

Inspired by a [reddit post](https://www.reddit.com/r/ProgrammerHumor/comments/6f1i7y/ah_fuck_how_am_i_meant_to_make_a_good_volume/), this is a **fake** ransomware

It *encrypts* your volume slider permanently to 100% until you make a payment to one of listed charity services.

You are also allowed to decrypt your volume to 90% **for free**

## Usage

### Windows withtout Python 3

1. just go to releases and download the exe installer
2. it will create a new folder on your desktop, open it and execute **RUN_ME.exe**

### Everything else except Mac

1. you are requied to have Python3 and pygame library installed
2. clone the source
3. launch **ransom_client.py**

### Mac

your're screwed, you cannot run this ransomware properly

## Credits

pygame buttons adapted from [http://simonhl.dk/button-drawer-python-2-6/](http://simonhl.dk/button-drawer-python-2-6/)

idea stolen from [https://i.redd.it/6iwmt9ojzf1z.jpg](https://i.redd.it/6iwmt9ojzf1z.jpg)

windows volume control is controled with [http://www.nirsoft.net/utils/nircmd.html](http://www.nirsoft.net/utils/nircmd.html)
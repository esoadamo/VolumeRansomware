AARP Foundation
Acorns Children's Hospice
Action Against Hunger
Action Deafness
ActionAid
Acumen
Adelson Foundation
Adventist Development and Relief Agency
Aerospace Heritage Foundation of Canada
Aleh Negev
Alex's Lemonade Stand Foundation
Alexander S. Onassis Foundation
Allegheny Foundation
The Alliance for Safe Children
Alpha Sigma Tau National Foundation, Inc.
American Academy in Rome
American Heart Association
American Himalayan Foundation
American India Foundation
American Indian College Fund
American Near East Refugee Aid
American Red Cross
Amici del Mondo World Friends Onlus
Amref Health Africa
Andrew W. Mellon Foundation
Artforum Culture Foundation
Asbestos Disease Awareness Organization
The Asia Foundation
Association L'APPEL
Association of Gospel Rescue Missions
Atlantic Philanthropies
Azim Premji Foundation
Baan Gerda
Best Friends Animal Society
Bharat Sevashram Sangha
Big Brothers Big Sisters of America
Big Brothers Big Sisters of Canada
Big Brothers Big Sisters of New York City
Bill & Melinda Gates Foundation
Bilqees Sarwar Foundation
Bloomberg Philanthropies
Born This Way Foundation
Boys & Girls Clubs of America
Bread for the World
Bremen Overseas Research and Development Association
British Heart Foundation
Burroughs Wellcome Fund
Bush Foundation
CAFOD
Calouste Gulbenkian Foundation
Campaign for Liberty
The Canadian International Learning Foundation
Cancer Research UK
CanTeen
Cardiac Risk in the Young
CARE
Caritas
Carter Center
Carthage Foundation
Casa Pia
Catholic Charities USA
Catholic Relief Services
CCIVS
Cesvi
Charitable Society of the Natives from Catalonia
Child In Need Institute
Child Watch Phuket
Child's Dream
Child's Play
Children at Risk
Children in Need
Children of Peace International
Children's Defense Fund
Children's Development Trust
The Children's Investment Fund Foundation
Children's Liver Disease Foundation
Children's Miracle Network Hospitals
Children's National Medical Center
Christian Blind Mission
Christian Care Foundation for Children with Disabilities
Christian Children's Fund of Canada
Church World Service
The Citizens Foundation
City Year
Clinton Foundation
Comenius Foundation for Child Development
Comic Relief
Community Network Projects
Compassion International
Confetti Foundation
Conrad N. Hilton Foundation
Counterpart International
The Crohn's and Colitis Foundation of Canada
Cystic Fibrosis Foundation
David and Lucile Packard Foundation
David Suzuki Foundation
Do Something
Dream Center
Drosos Foundation
The Duke Endowment
East Meets West
Edhi Foundation
Engineers Without Borders
Epilepsy Foundation
Epilepsy Outlook
Eppley Foundation
Feeding America
Feed My Starving Children
FINCA International
Food Allergy Initiative
Ford Foundation
The Foundation for a Better Life
Foundation for Child Development
The Fred Hollows Foundation
Free the Children
Fremont Area Community Foundation
Friedrich Ebert Stiftung
Fritt Ord
Fund for Reconciliation and Development
Fundacion Manantiales
Garfield Weston Foundation
GEFEK
George S. and Dolores Dore Eccles Foundation
German Foundation for World Population
Gill Foundation
Girl Scouts of the USA
GiveIndia
The Global Fund to Fight AIDS, Tuberculosis and Malaria
Global Greengrants Fund
Global Village Foundation
Gordon and Betty Moore Foundation
Grassroots Business Fund
Greenpeace
Habitat for Humanity
Handicap International
Hands on Network
Hands on Tzedakah
Heal the World Foundation
Heart to Heart International
Heifer International
Helen Keller International
Heritage Action
The Heritage Foundation
High Fives Foundation
Holt International Children's Services
Howard Hughes Medical Institute
IHH (Turkish NGO)
Illinois Prairie Community Foundation
Imam Khomeini Relief Foundation
International Children Assistance Network
International Crane Foundation
International Federation of Red Cross and Red Crescent Societies
International Foundation for Electoral Systems
International Fund for Animal Welfare
International Literacy Foundation
International Medical Corps
International Planned Parenthood Federation
International Republican Institute
International Resources for the Improvement of Sight
International Trachoma Initiative
International Union for Conservation of Nature
Invisible Children, Inc.
Islamic Relief
ISSO Seva
J. Paul Getty Trust
Jain Foundation
Jane Goodall Institute
Jesse's Journey
Jesuit Refugee Service
Jewish Community Center
John A. Hartford Foundation
John D. and Catherine T. MacArthur Foundation
Just a Drop
Kahrizak Charity Foundation
Kin Canada
Knut and Alice Wallenberg Foundation
Konrad Adenauer Foundation
The Kresge Foundation
La Caixa
Laidlaw Foundation
The Lawrence Foundation
LDS Humanitarian Services
Legal Education Foundation
The Leona M. and Harry B. Helmsley Charitable Trust
Lepra
Li Ka Shing Foundation
Libra Foundation
Lifeline Express
Lilly Endowment
Lions Clubs International
Make-A-Wish Foundation
Malteser International
Marie Stopes International
The MasterCard Foundation
Material World Charitable Foundation
Maybach Foundation
McKnight Foundation
Médecins du Monde
Médecins Sans Frontières (Doctors Without Borders)
Mercy Corps
Mercy International Foundation
Michael and Susan Dell Foundation
Mohammed bin Rashid Al Maktoum Foundation
Movimento Sviluppo e Pace
Mukti
Multiple Sclerosis Foundation
Nemours Foundation
Netherlands Leprosy Relief
Network for Good
The New Brunswick Innovation Foundation
Nimmagadda Foundation
Nippon Foundation
Nobel Foundation
Norwegian Mission Alliance
NYRR Foundation
The Oaktree Foundation
Omar-Sultan Foundation
ONS Foundation
Operation Blessing International
Operation Smile
Operation USA
Opportunity International
Orbis International
Ormiston Trust
Oxfam
The Pew Charitable Trusts
Pin-ups for Vets
Plan
Ratanak International
Realdania
Reggio Children Foundation
REHASWISS
Robert Bosch Stiftung
Robert Wood Johnson Foundation
Rockefeller Brothers Fund
Rockefeller Foundation
Rotary Foundation
Royal Flying Doctor Service of Australia
Royal London Society for Blind People
Royal Society for the Prevention of Cruelty to Animals
Royal Society for the Protection of Birds
Saint Camillus Foundation
The Salvation Army
Samaritan's Purse
Samsara Foundation
Santa Casa da Misericórdia
Save the Children
Save the Manatee Club
Scaife Family Foundation
SCARE for a CURE
Schowalter Foundation
Silicon Valley Community Foundation
Simone and Cino Del Duca Foundation
Sir Dorabji Tata and Allied Trusts
SKIP of New York
SmileTrain
Society of Saint Vincent de Paul
Somaly Mam Foundation
SOS Children's Villages
SOS Children's Villages – UK
SOS Children's Villages – USA
Sparebankstiftelsen DnB
St. Baldrick's Foundation
St. Jude Children's Research Hospital
Starlight Children's Foundation
Stichting INGKA Foundation
Students Helping Honduras
Surdna Foundation
Survivor Corps
Susan G. Komen for the Cure
SystemX
Taproot Foundation
Thrive Africa
Tom Joyner Foundation
Toys for Tots
Traffic
Tulsa Community Foundation
UNICEF
United Methodist Committee on Relief
United Nations Foundation
United States Artists
United Way Worldwide
Universal Health Care Foundation of Connecticut
Vancouver Foundation
Varkey Foundation
Veniños
Vietnam Children's Fund
Vietnam Veterans Memorial Fund
Vietnam Veterans of America Foundation
Virtu Foundation
Voluntary Service Overseas
Volunteers of America
The W. Garfield Weston Foundation
W. K. Kellogg Foundation
The Walter and Duncan Gordon Foundation
Waste No Food
Water Agriculture and Health in Tropical Area
Wellcome Trust
The Weston Foundation
Wetlands International
Wikimedia Foundation
WildAid
Wildlife Conservation Society
William and Flora Hewlett Foundation
The Winnipeg Foundation
Woodrow Wilson National Fellowship Foundation
World Association of Girl Guides and Girl Scouts
World Literacy Foundation
World Medical Relief
World Scout Foundation
World Transformation Movement
World Vision International
World Wide Fund for Nature
Wounded Warrior Project
Wyoming Wildlife Federation
Young Lives
Youth With A Mission
YouthBuild
import pygame
import volume_control as volume
import random
import datetime
import Buttons


EXIT_PROGRAM = False


def main_client():
    global EXIT_PROGRAM
    pygame.init()
    screen = pygame.display.set_mode((1000, 550))
    clock = pygame.time.Clock()

    pygame.display.set_icon(pygame.image.load('skull.ico'))

    target_volume = 1

    price = random.randint(100, 1000)
    with open('charits.txt', 'rt') as f:
        c = f.read()
        c = c.splitlines()
        charity = c[random.randint(0, len(c))]
        del c

    timestart = datetime.datetime.now()

    button_free_decrypt = Buttons.Button()
    button_free_decrypt_text = "Free decrypt to 90%"

    button_paid = Buttons.Button()
    button_paid_text = "I have paid already"
    click_count = 0

    pygame.display.set_caption("VolumeRansomware")

    while not EXIT_PROGRAM:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                # EXIT_PROGRAM = True
                pass
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if target_volume == 1 and button_free_decrypt.pressed(pygame.mouse.get_pos()):
                    button_free_decrypt_text = "Decrypted to 90%"
                    target_volume = 0.9
                if button_paid.pressed(pygame.mouse.get_pos()):
                    click_count += 1
                    if click_count == 1:
                        button_paid_text = "NO"
                    elif click_count == 2:
                        button_paid_text = "Nope"
                    elif click_count == 3:
                        button_paid_text = "No way"
                    elif click_count == 4:
                        button_paid_text = "I do NOT trust you"
                    elif click_count == 5:
                        button_paid_text = "Really?"
                    elif click_count == 6:
                        button_paid_text = "Still not sure"
                    elif click_count == 7:
                        button_paid_text = "Maybe one more"
                    elif click_count == 8:
                        button_paid_text = "Ok, I trust you"
                    elif click_count == 9:
                        button_paid_text = "Enjoy your volume control"
                    elif click_count == 10:
                        EXIT_PROGRAM = True

        screen.fill((253, 54, 18))

        font = pygame.font.SysFont("Arial Bold", 60)
        label = font.render("Your volume control has been encrypted!", 1, (22, 109, 32))
        screen.blit(label, (10, 10))

        lines = ["Hey mate, you just launched this ransomware. Thank you.",
                 "From now on, you have 6 hours to pay %d $ to charity." % price,
                 "Charity selected for you is: %s" % charity,
                 "If you will obey with the payment your volume will remain on 100% forever",
                 "Thank you for your charity support ;-)",
                 "To prove that you can trust me I can decrypt your volume to 90% for free"]

        font = pygame.font.SysFont("Arial Bold", 30)

        y = 75
        for line in lines:
            label = font.render(line, 1, (38, 18, 253))
            screen.blit(label, (10, y))
            y += 30

        y += 60
        line = "Time left: %d" % ((6 * 3600) - (datetime.datetime.now() - timestart).seconds)
        font = pygame.font.SysFont("Arial Bold", 80)
        label = font.render(line, 1, (27, 5, 19))
        screen.blit(label, (250, y))

        y += 100

        button_free_decrypt.create_button(screen, (107, 142, 35), 0, y, 500, 100, 0,
                                          button_free_decrypt_text, (255, 255, 255))

        button_paid.create_button(screen, (133, 15, 250), 550, y, 500, 100, 0,
                                  button_paid_text, (255, 0, 0))

        pygame.display.flip()
        clock.tick(30)

        volume.set_volume(target_volume)
        volume.mute_volume(False)

if __name__ == '__main__':
    main_client()
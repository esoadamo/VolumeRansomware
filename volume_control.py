import subprocess
import os.path
import sys

VOLUME_MAX = 65535
NIRCMD_PATH = "nircmd/nircmd.exe"

if sys.platform.startswith('lin'):
    PLATFORM = "LINUX"
else:
    PLATFORM = "WINDOWS"


def set_volume(value):
    """
    Sets windows volume to some value
    :param value: 0.0 to 1.0
    :return: None
    """
    if PLATFORM == "LINUX":
        command = ['amixer', '-D', 'pulse', 'sset', 'Master', str(int(value * 100)) + '%']
    else:
        exe_path = os.path.abspath(NIRCMD_PATH)
        command = [exe_path, 'setsysvolume', str(int(value * VOLUME_MAX))]
    subprocess.call(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def mute_volume(bool_mute):
    """
    (un)mutes the system volume
    :param bool_mute: True - mute, False - unmute
    :return: False
    """
    if PLATFORM == "LINUX":
        command = ['amixer', '-D', 'pulse', 'set', 'Master', '1+', 'off' if bool_mute else 'on' ]
    else:
        exe_path = os.path.abspath(NIRCMD_PATH)
        command = [exe_path, 'mutesysvolume', str(int(bool_mute))]
    subprocess.call(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
